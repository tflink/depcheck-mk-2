# depcheck.py - core logic and interfaces for checking dependency trees
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Copyright 2013, 2014 John H. Dulaney
# Copyright 2014 Red Hat, Inc
#
# Authors:
#   John H. Dulaney <jdulaney@fedoraproject.org>
#   Tim Flink <tflink@redhat.com>
#
# EPEL dep:  python-argparse


import solv
import os
import tempfile

from .output import format_out
from . import Pass, Fail

config_file = "/etc/depcheck.conf"

# TODO: handle configuration file

# FIXME: this will eventually be a util in libtaskotron
def rpm_filename_to_envr(filename):
    return os.path.basename(filename)[:-4]


class Depcheck(object):
    def __init__(self, arch, repodata, workdir=None):
        """Depcheck uses existing repodata to check a set of rpm files for
        depsolving issues.

        This class itself doesn't handle much other than checking rpms and all
        data prep needs to be handled externally

        :param arch: The repository arch to use for dependency solving
        :type arch: str
        :param repodata: local paths to downloaded and verified repodata
        :type repodata: dir of dir {'reponame': {'primary': primarygz_filename, 'filelists': filelists_filename}...}
        :param workdir: The directory to use for depcheck work, a temp directory will be created if workdir is None
        :type workdir: str
        """

        self.arch = arch
        self.repos = repodata
        self.solvpool_object = None
        self.solvrepo_object = None
        self.run_result = []

        if workdir is None:
            self.workdir = tempfile.mkdtemp('', 'depcheck-', '/var/tmp/')
        else:
            self.workdir = workdir

    @property
    def solvpool(self):
        if self.solvpool_object is None:
            self.solvpool_object = solv.Pool()
            self.solvpool_object.setarch(self.arch)
        return self.solvpool_object

    @property
    def solvrepo(self):
        if self.solvrepo_object is None:
            self.solvrepo_object = self.solvpool.add_repo(self.arch)
        return self.solvrepo_object

    def _prepare_libsolv(self):
        """Initialize libsolv and add the needed repository information for
        checking dependencies for the supplied repositories
        """

        for repo in self.repos:
            primary = open(self.repos[repo]['primary'], 'rb')
            filelist = open(self.repos[repo]['filelists'], 'rb')

            self.solvrepo.add_rpmmd(primary, None)
            self.solvrepo.add_rpmmd(filelist, None,
                                    solv.Repo.REPO_EXTEND_SOLVABLES)

    def _check_rpm(self, rpmfile):
        """Check a single rpm file against prepared repodata
         :param rpmfile: filename of rpm file to check
         :type rpmfile: str
        """
        solvable = self.solvrepo.add_rpm(rpmfile)
        self.solvpool.addfileprovides()
        self.solvpool.createwhatprovides()

        job = self.solvpool.Job(solv.Job.SOLVER_INSTALL | solv.Job.SOLVER_SOLVABLE,
                                solvable.id)

        # important note - make sure to keep the solver in the same scope as
        # the problem analysis code or else python-solv segfults will ensue
        solvpool_solver = self.solvpool.Solver()
        solv_problems = solvpool_solver.solve([job])

        if len(solv_problems) == 0:
            return Pass, []

        else:
            problem_strs = []
            for problem in solv_problems:
                probrules = problem.findallproblemrules()

                for rule in probrules:
                    ruleinfos = rule.allinfos()
                    for info in ruleinfos:
                        problem_strs.append(info.problemstr())

            return Fail, problem_strs

    def check_rpm_deps(self, rpmfiles):
        """Check dependencies of the given rpms against supplied repodata

        :param rpmfiles: local absolute paths to rpms to check
        :type rpmfiles: list of str
        """

        self._prepare_libsolv()

        for rpmfile in rpmfiles:
            print 'running depcheck for %s' % rpmfile
            result_state, result_details = self._check_rpm(rpmfile)

            self.run_result.append({'envr': rpm_filename_to_envr(rpmfile),
                                    'result': result_state,
                                    'results': result_details})

    def get_output(self, output_format='tap'):
        """Get formatted run result output. Will return empty if do_depcheck has
        not yet been run

        :param output_format: output format to use, can be one of 'tap' or 'json'
        :type output_format: str
        :returns: formatted run result
        """

        return format_out(self.run_result, output_format)


def run(rpms, arch, release, repos, out_format):
    # for the moment, make the following assumptions:
    # - arch is specified and correct
    # - all repos are passed in the form of local paths
    # - output format is tap
    # - all rpms are passed in as a list - no dirs

    # setup solv pool
    # collate results
    pass
