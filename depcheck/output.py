# Copyright 2014 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Depcheck mk 2

import json
from . import Pass, Fail


def json_out(results):
    """Generate JSON output"""
    mod_results = []
    for result in results:
        if result['result'] == Pass:
            result['result'] = 'Pass'
        else:
            result['result'] = 'Fail'
        mod_results = mod_results + [result]

    jsongen = json.dumps(results, indent=4, separators=(',', ': '))
    return jsongen


def tap_out(results):
    """Generate TAP output"""
    try:
        from libtaskotron import tap
    except ImportError:
        raise Exception("libtaskotron not present")

    tapgen = tap.TAPGenerator()
    tapout = []
    for result in results:
        tapout.append(tapgen.format_TAP_msg(result['result'], 'depcheck: %s' % result['envr'],
                                            desc='depcheck  results for %s' % result['envr'],
                                            data={'output': result['results']}))

    return tapout


def format_out(results, out_format):
    """We have our output, now to format it"""

    if out_format == "tap":
        return tap_out(results)
    elif out_format == "json":
        return json_out(results)
