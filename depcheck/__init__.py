#!/usr/bin/python
#
# Copyright 2013 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Depcheck mk 2
# EPEL dep:  python-argparse
"""Depcheck mk 2 is a new dependency checker for rpms and yum repositories"""

__version__ = '0.0.5'


# Check to see if libtaskotron is present; if so, import pass and fail

try:
    from libtaskotron import tap

    Pass = tap.PASS
    Fail = tap.FAIL
except ImportError:
    Pass = 0
    Fail = 1

import os.path
import sys
import argparse
import tempfile
import platform

from . import depcheck
from . import metadata


def dir_to_rpms(dirname):
    """Find all rpms in the specified directory
    :param dirname: local directory in which to look for rpms
    :type dirnema: str
    :returns: list of rpm files with absolute, local path
    """
    rpm_list = []
    for filename in os.listdir(dirname):
        if filename.endswith('.rpm'):
            rpm_list.append(os.path.join(dirname, filename))
    return rpm_list


def extract_named_repo(namedrepo):
    return namedrepo.split(':', 1)


def depcheck_cli():
    parser = argparse.ArgumentParser()
    parser.add_argument("rpm", nargs='+', help="RPM to be tested")
    parser.add_argument("-a", "--arch", choices=["i386", "x86_64", "armhfp"], help="""
                        Architecture to be tested; support arches are x86, x86_64, and armhfp.
                        If omitted, defaults to current machine architecture.
                        """)
    parser.add_argument("-r", "--repos", nargs='+', action='store', help="""
                        Repo to be tested against, in the form of reponame:repolocation where
                        repolocation can be local (in the form /dir/of/repo) or
                        remote (direct link or standard metalink), to be tested against.
                      """)
    parser.add_argument("--release", help="Fedora release to be tested")
    parser.add_argument("-f", "--format", choices=["json", "tap"], help="""
                        Specify output format to override configuration
                        """)

    args = parser.parse_args()

    rpmlist = []
    # handle directory args, expanding them to a list of rpm files with absolute
    # paths
    for filename in args.rpm:
        if os.path.isdir(filename):
            rpmlist.extend(dir_to_rpms(filename))
        else:
            rpmlist.append(filename)

    # autodetect arch if none is passed in
    if args.arch is None:
        arch = platform.machine()
    else:
        arch = args.arch

    # arm has some special case requirements for arch handling
    # Fixup for arm; the yum repo is labled as armhfp but the envr is armv7hl
    if arch == 'armv7l':
        arch = 'armhfp'

    # handle data formatting for repo metadata (list of dicts)
    repos = {}
    for repo in args.repos:
        namedrepo = extract_named_repo(repo)
        repos[namedrepo[0]] = {'arch': args.arch, 'path': namedrepo[1]}

    workdir = tempfile.mkdtemp('', 'depcheck-', '/var/tmp/')
    md_handle = metadata.DepcheckMetadata(repos, targetdir=workdir)

    repomd_files = md_handle.repodata

    output = []
    try:
        depck = depcheck.Depcheck(arch, repomd_files, workdir=workdir)
        depck.check_rpm_deps(rpmlist)
        output = depck.get_output(output_format=args.format)
    except KeyboardInterrupt, e:
        print >> sys.stderr, ("\n\nExiting on user cancel.")
        sys.exit(1)

    print output
