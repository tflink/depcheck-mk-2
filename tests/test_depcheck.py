from dingus import Dingus
import __builtin__

from depcheck import depcheck


class TestDepcheckLibsolvPrep(object):
    def test_libsolv_setup_adds_repos(self, monkeypatch):
        ref_arch = 'x86_64'
        ref_basepath = '/path/to/testing-repo/'
        ref_repomdfiles = {'reponame': {'primary': '%sprimary.xml.gz' % ref_basepath,
                                        'filelists': '%sfilielists.xml.gz' % ref_basepath}}

        stub_solvpool = Dingus('solvpool')
        stub_solvrepo = Dingus('solvrepo')
        stub_open = Dingus('open')
        test_depcheck = depcheck.Depcheck(ref_arch, ref_repomdfiles)
        test_depcheck.solvpool_object = stub_solvpool
        test_depcheck.solvrepo_object = stub_solvrepo

        monkeypatch.setattr(__builtin__, 'open', stub_open)

        test_depcheck._prepare_libsolv()

        assert len(stub_solvrepo.calls()) == 2

