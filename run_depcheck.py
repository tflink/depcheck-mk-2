#!/usr/bin/python

import depcheck
import os

"""Entry point for depcheck"""


if __name__ == '__main__':
    depcheck.depcheck_cli()
